#!/usr/bin/env bash

cd /app
composer install
php ./init --env=Development --overwrite=n
/app/yii cache/flush-all
#/app/yii ide-components
/app/yii migrate --interactive=0
#/app/yii_test migrate --interactive=0
