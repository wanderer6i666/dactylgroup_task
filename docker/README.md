Mysql
=======
```bash
docker-compose exec mysql bash
mysql -uroot -pverysecret app
```

Заливка базы (на host системе):
```bash
docker exec -i dtask_mysql_1 sh -c 'exec zcat | mysql -u root -p"$MYSQL_ROOT_PASSWORD" -A -Dapp' < ./console/runtime/app_new.sql.gz
```

Экспорт базы:
```bash
docker-compose exec mysql /bin/bash -c 'mysqldump --single-transaction --complete-insert --add-drop-table --quick --quote-names --hex-blob -uroot -p app | gzip > /dumps/output_dump.sql.gz'
```

Composer
=======
```bash
docker-compose run --rm php-fpm composer update
```

Codeception
=======
```bash
docker-compose run --rm php-fpm vendor/bin/codecept run
```

Sphinx
======

Первое создание индексов:

```bash
docker-compose run --rm sphinx indexer --all --config /opt/sphinx/conf/sphinx.conf
```

Обновление индекса:

```bash
docker-compose exec sphinx indexer --all --rotate --config /opt/sphinx/conf/sphinx.conf
```

Node.js
=======

Зайти в консоль:

```bash
docker-compose run --rm node bash
```

Выполнить gulp:

```bash
docker-compose run --rm node gulp
```

# How to run #

Dependencies:

  * Docker engine v1.13 or higher. Your OS provided package might be a little old, if you encounter problems, do upgrade. See [https://docs.docker.com/engine/installation](https://docs.docker.com/engine/installation)
  * Docker compose v1.12 or higher. See [docs.docker.com/compose/install](https://docs.docker.com/compose/install/)

Once you're done, simply `cd` to your project and run `docker-compose up -d`. This will initialise and start all the containers, then leave them running in the background.

# Docker compose cheatsheet #

**Note:** you need to cd first to where your docker-compose.yml file lives.

  * Start containers in the background: `docker-compose up -d`
  * Start containers on the foreground: `docker-compose up`. You will see a stream of logs for every container running.
  * Stop containers: `docker-compose stop`
  * Kill containers: `docker-compose kill`
  * View container logs: `docker-compose logs`
  * Execute command inside of container: `docker-compose exec SERVICE_NAME COMMAND` where `COMMAND` is whatever you want to run. Examples:
        * Shell into the PHP container, `docker-compose exec php-fpm bash`
        * Run symfony console, `docker-compose exec php-fpm bin/console`
        * Open a mysql shell, `docker-compose exec mysql mysql -uroot -pCHOSEN_ROOT_PASSWORD`

# Application file permissions #

As in all server environments, your application needs the correct file permissions to work proberly. You can change the files throught the container, so you won't care if the user exists or has the same ID on your host.

`docker-compose exec php-fpm chown -R www-data:www-data /application/public`

# Recommendations #

It's hard to avoid file permission issues when fiddling about with containers due to the fact that, from your OS point of view, any files created within the container are owned by the process that runs the docker engine (this is usually root). Different OS will also have different problems, for instance you can run stuff in containers using `docker exec -it -u $(id -u):$(id -g) CONTAINER_NAME COMMAND` to force your current user ID into the process, but this will only work if your host OS is Linux, not mac. Follow a couple of simple rules and save yourself a world of hurt.

  * Run composer outside of the php container, as doing so would install all your dependencies owned by `root` within your vendor folder.
  * Run commands (ie Symfony's console, or Laravel's artisan) straight inside of your container. You can easily open a shell as described above and do your thing from there.

# Simple basic Xdebug configuration with integration to PHPStorm

## To config xdebug you need add these lines in php-fpm/php-ini-overrides.ini:

### For linux:
```
xdebug.remote_enable=1
xdebug.remote_connect_back=1
xdebug.remote_autostart = 1
```

### For MacOS and Windows:
```
xdebug.remote_enable=1
xdebug.remote_host=host.docker.internal
xdebug.remote_autostart = 1
```

## Add the section “environment” to the php-fpm service in docker-compose.yml:

environment:

  PHP_IDE_CONFIG: "serverName=Docker"

### Create a server configuration in PHPStorm:
 * In PHPStorm open Preferences | Languages & Frameworks | PHP | Servers
 * Add new server
 * The “Name” field should be the same as the parameter “serverName” in “environment” in docker-compose.yml
 * A value of the "port" field should be the same as first port(before a colon) in "webserver" service in docker-compose.yml
 * Select "Use path mappings" and set mappings between a path to your project on a host system and the Docker container.
 * Finally, add “Xdebug helper” extension in your browser, set breakpoints and start debugging



