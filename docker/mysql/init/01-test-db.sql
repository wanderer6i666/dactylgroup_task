# create databases
CREATE DATABASE IF NOT EXISTS `app_test`;

# grant rights
GRANT ALL ON *.* TO 'app'@'%';
