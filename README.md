<p align="center">
    <h1 align="center">Test task</h1>
    <br>
</p>

Sutup with Docker
-------------------

```
docker-compose build --no-cache

docker-compose up -d

docker-compose run --rm php-fpm bash

composer install

php init

cp common/config/main-local-example.php common/config/main-local.php

php yii migrate

php yii restaurant/import

```

Docker documentation is at [docker/README.md](docker/README.md).