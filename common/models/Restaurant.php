<?php

namespace common\models;

use common\models\query\RestaurantQuery;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "restaurant".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $cuisine
 * @property int $price
 * @property int $rating
 * @property string $location
 * @property string $description
 * @property int $status
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property RestaurantSchedule[] $restaurantSchedule
 */
class Restaurant extends ActiveRecord
{
    public static function tableName(): string
    {
        return '{{%restaurant}}';
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class,
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'code',
                'slugAttribute' => 'code',
            ],
        ];
    }

    public function rules(): array
    {
        return [
            [['name', 'code', 'cuisine', 'location', 'description'], 'filter', 'filter' => 'strip_tags'],
            [['name', 'code', 'cuisine', 'location', 'description'], 'trim'],
            [['name', 'code'], 'required'],
            [['name', 'code'], 'unique'],
            [['price', 'rating', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'cuisine', 'location', 'description'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 100],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'cuisine' => 'Cuisine',
            'price' => 'Price',
            'rating' => 'Rating',
            'location' => 'Location',
            'description' => 'Description',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getRestaurantSchedule(): ActiveQuery
    {
        return $this->hasMany(RestaurantSchedule::class, ['restaurant_id' => 'id']);
    }

    public static function find(): ActiveQuery
    {
        return new RestaurantQuery(get_called_class());
    }
}
