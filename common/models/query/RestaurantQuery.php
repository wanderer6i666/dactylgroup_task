<?php

namespace common\models\query;

use common\helpers\RestaurantHelper;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Restaurant]].
 *
 * @see \common\models\Restaurant
 */
class RestaurantQuery extends ActiveQuery
{
    public function currentlyOpened()
    {
        $day = RestaurantHelper::formatDay(date('D'));
        $time = date("H:i:s");

        $condition = '(rs.opens < rs.closes AND rs.opens < :time AND rs.closes > :time) OR (rs.opens > rs.closes AND (rs.opens < :time OR rs.closes > :time))';

        return $this->innerJoinWith('restaurantSchedule rs', false)
            ->andWhere(['rs.day' => $day])
            ->andWhere($condition, [
                'time' => $time,
            ]);
    }
}
