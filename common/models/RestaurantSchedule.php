<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "restaurant_schedule".
 *
 * @property int $id
 * @property int $restaurant_id
 * @property string $day
 * @property string|null $opens
 * @property string|null $closes
 *
 * @property Restaurant $restaurant
 */
class RestaurantSchedule extends ActiveRecord
{
    public static array $days = [
        'su',
        'mo',
        'tu',
        'we',
        'th',
        'fr',
        'sa',
    ];

    public static function tableName(): string
    {
        return '{{%restaurant_schedule}}';
    }

    public function rules(): array
    {
        return [
            [['restaurant_id', 'day'], 'required'],
            [['restaurant_id'], 'integer'],
            [['opens', 'closes'], 'date', 'format' => 'HH:mm:ss'],
            [['day'], 'string', 'max' => 2],
            ['day', 'unique', 'targetAttribute' => ['restaurant_id', 'day'], 'message' => 'Schedule for this day is already exists'],
            ['day', 'in', 'range' => self::$days],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurant::class, 'targetAttribute' => ['restaurant_id' => 'id']],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'restaurant_id' => 'Restaurant ID',
            'day' => 'Day',
            'opens' => 'Opens',
            'closes' => 'Closes',
        ];
    }

    public function getRestaurant(): ActiveQuery
    {
        return $this->hasOne(Restaurant::class, ['id' => 'restaurant_id']);
    }
}
