<?php

namespace common\helpers;

use common\models\RestaurantSchedule;
use Exception;

class RestaurantHelper
{
    /**
     * @param string $day Week day name
     *
     * @return string (week day name 2 char length)
     */
    public static function formatDay(string $day)
    {
        return strtolower(substr(trim($day), 0, 2));
    }

    /**
     * Format time from fomat 10am to 10:00:00
     *
     * @param string $time
     *
     * @return string
     * @throws Exception
     */
    public static function formatTime(string $time)
    {
        if (strpos($time, 'am') === false && strpos($time, 'pm') === false) {
            throw new Exception('Invalid time format');
        }

        return date("H:i:00", strtotime($time));
    }


    /**
     * @param RestaurantSchedule[] $schedule
     *
     * @return false
     */
    public static function getOpenStatusFromSchedule(array $schedule)
    {
        $isOpen = false;
        $day = static::formatDay(date('D'));
        $time = time();

        foreach ($schedule as $daySchedule) {
            if ($daySchedule->day != $day) {
                continue;
            }

            $timeOpens =  strtotime($daySchedule->opens);
            $timeCloses =  strtotime($daySchedule->closes);

            if (
                ($timeOpens > $timeCloses && ($time > $timeOpens || $time < $timeCloses)) ||
                ($timeOpens < $timeCloses && ($time > $timeOpens && $time < $timeCloses))
            ) {
                $isOpen = true;
            }
        }

        return $isOpen;
    }
}
