<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

defined('UTC_MINUTE') || define('UTC_MINUTE', 60);
defined('UTC_HOUR') || define('UTC_HOUR', 3600);
defined('UTC_DAY') || define('UTC_DAY', 86400);