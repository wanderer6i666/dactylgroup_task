<?php

use yii\db\Connection;

return [
    'components' => [
        'db' => [
            'class' => Connection::class,
            'dsn' => 'mysql:host=mysql;dbname=app',
            'username' => 'root',
            'password' => 'verysecret',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
            'schemaCacheDuration' => UTC_HOUR,
            'schemaCache' => 'cache',
        ],
//        'cache' => [
//            'servers' => [
//                'main' => [
//                    'host' => 'memcached',
//                ],
//            ],
//        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];
