<?php

use common\helpers\RestaurantHelper;
use common\models\Restaurant;
use frontend\models\RestaurantSearch;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $searchModel RestaurantSearch
 * @var $dataProvider ActiveDataProvider
 */

$this->title = 'Restaurants';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="restaurant-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="text-center">
        <h3>By dafault display only currently opened restaurants.</h3>
        <h3>But when you search something it search and display closed restaurants too.</h3>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'code',
            'cuisine',
            'price',
            'rating',
            'location',
            'description',
            [
                'label' => 'Schedule',
                'format' => 'raw',
                'value' => function (Restaurant $model) {
                    $schedule = '';

                    $isOpen = RestaurantHelper::getOpenStatusFromSchedule($model->restaurantSchedule);

                    foreach ($model->restaurantSchedule as $daySchedule) {
                        $schedule .= Yii::t('app', '{day} {opens}-{closes}', [
                                'day' => ucfirst($daySchedule->day),
                                'opens' => date('H:i', strtotime($daySchedule->opens)),
                                'closes' => date('H:i', strtotime($daySchedule->closes)),
                            ]) . '<br>';
                    }

                    if ($isOpen) {
                        $schedule .= Html::tag('span', 'Open', ['class' => 'label label-success']);
                    } else {
                        $schedule .= Html::tag('span', 'Close', ['class' => 'label label-danger']);
                    }

                    return $schedule;
                },
            ],
        ],
    ]); ?>
</div>
