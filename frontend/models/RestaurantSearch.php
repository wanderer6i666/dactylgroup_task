<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Restaurant;

/**
 * RestaurantSearch represents the model behind the search form of `common\models\Restaurant`.
 */
class RestaurantSearch extends Restaurant
{
    public function rules(): array
    {
        return [
            [['name', 'code', 'cuisine', 'location', 'description'], 'filter', 'filter' => 'strip_tags'],
            [['name', 'code', 'cuisine', 'location', 'description'], 'trim'],
            [['name', 'cuisine', 'location', 'description'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 100],
            [['price', 'rating'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Restaurant::find()
            ->with(['restaurantSchedule']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $isShowOnlyOpened = true;

        if ($this->load($params)) {
            foreach ($params['RestaurantSearch'] as $param) {
                if ($param !== null && $param !== '') {
                    $isShowOnlyOpened = false;
                }
            }
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($isShowOnlyOpened) {
            $query->currentlyOpened();
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'price' => $this->price,
            'rating' => $this->rating,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'cuisine', $this->cuisine])
            ->andFilterWhere(['like', 'location', $this->location])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
