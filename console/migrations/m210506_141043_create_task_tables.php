<?php

use console\components\Migration;

/**
 * Class m210506_141043_create_task_tables
 */
class m210506_141043_create_task_tables extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%restaurant}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string()->notNull()->unique(),
            'code' => $this->string(100)->notNull()->unique(),
            'cuisine' => $this->string()->notNull()->defaultValue(''),
            'price' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'rating' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'location' => $this->string()->notNull()->defaultValue(''),
            'description' => $this->string()->notNull()->defaultValue(''),
            'status' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'created_at' => $this->unixTimestamp(),
            'updated_at' => $this->unixTimestamp(),
        ]);

        $this->createTable('{{%restaurant_schedule}}', [
            'id' => $this->primaryKey()->unsigned(),
            'restaurant_id' => $this->integer()->unsigned()->notNull(),
            'day' => $this->string(2)->notNull(),
            'opens' => $this->time(),
            'closes' => $this->time(),
        ]);

        $this->createIndex(
            'idx-rs-restaurant_id',
            'restaurant_schedule',
            'restaurant_id'
        );

        $this->addForeignKey(
            'fk-rs-restaurant_id',
            'restaurant_schedule',
            'restaurant_id',
            'restaurant',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%restaurant_schedule}}');
        $this->dropTable('{{%restaurant}}');
    }
}
