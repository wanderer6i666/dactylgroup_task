<?php

namespace console\helpers;

use common\helpers\RestaurantHelper;
use common\models\RestaurantSchedule;
use Exception;
use ruskid\csvimporter\CSVReader;
use Yii;
use yii\helpers\ArrayHelper;

class RestaurantImportHelper
{
    /**
     * @param string $importFile Path to file
     * @param array $requiredCols Required column names
     * @param int $startFromLine
     *
     * @return array
     * @throws Exception
     */
    public static function readFileCsv(string $importFile, array $requiredCols = [], int $startFromLine = 0): array
    {
        if (!static::isValidFileCsv($importFile)) {
            throw new Exception('Invalid file format');
        }

        $reader = new CSVReader([
            'filename' => $importFile,
            'fgetcsvOptions' => [
                'delimiter' => ',',
            ],
            'startFromLine' => $startFromLine,
        ]);

        $data = $reader->readFile();

        if (!$data) {
            throw new Exception('No data was found in file');
        }

        if ($requiredCols) {
            // All column names
            $colNames = ArrayHelper::getValue($data, 0, []);
            $notFindedCols = array_diff($requiredCols, $colNames);

            if ($notFindedCols) {
                throw new Exception(Yii::t('app', 'Required colums are not found in file {importFile} {notFindedCols}', [
                    'importFile' => $importFile,
                    'notFindedCols' => print_r($notFindedCols, true),
                ]));
            }
        }

        return $data;
    }

    /**
     * Validate CSV file
     *
     * @param string $importFile Path to csv file
     *
     * @return bool
     */
    public static function isValidFileCsv(string $importFile): bool
    {
        $filename = basename($importFile);

        preg_match('|\.([a-z0-9]{2,4})$|i', $filename, $fileSuffix);

        return (isset($fileSuffix[1]) && strtolower($fileSuffix[1]) == 'csv');
    }

    /**
     * @param string $daysTime "Mon-Thu, Sun 11:30 am - 9 pm"
     *
     * @return array  Array with short names of week days likr ['su', 'mo', 'tu']
     * @throws Exception
     */
    public static function getDaysFromString(string $daysTime)
    {
        $daysTime = trim($daysTime);

        // Get days from string
        preg_match('/\b((\w{3}-\w{3})|(\w{3}))(\W{2})*(\w{3}-\w{3})*(\w{3})*\b/', $daysTime, $daysMatches);
        $daysMatches = array_filter(array_unique($daysMatches), function ($var) {
            return (!empty($var) && strpos($var, ',') === false);
        });


        if (!$daysMatches) {
            throw new Exception('No valid date found');
        }

        $daysAll = RestaurantSchedule::$days;

        $daysPeriod = [];

        // Getting valid days
        foreach ($daysMatches as $days) {
            $days = explode('-', $days);
            $dayOne = RestaurantHelper::formatDay($days[0]);
            $dayTwo = RestaurantHelper::formatDay(ArrayHelper::getValue($days, 1, ''));

            if ($dayTwo) {
                $kStart = array_search($dayOne, $daysAll);
                $kEnd = array_search($dayTwo, $daysAll);

                foreach ($daysAll as $k => $day) {
                    if (
                        ($kStart > $kEnd && ($k >= $kStart || $k <= $kEnd)) ||
                        ($kStart < $kEnd && ($k >= $kStart && $k <= $kEnd))
                    ) {
                        $daysPeriod[] = $day;
                    }
                }
            } else {
                $daysPeriod[] = $dayOne;
            }
        }

        $daysPeriod = array_unique($daysPeriod);

        if (!$daysPeriod) {
            throw new Exception('No valid date found');
        }

        return $daysPeriod;
    }

    /**
     * @param string $daysTime "Mon-Thu, Sun 11:30 am - 9 pm"
     *
     * @return string Time interval in format 00:00:00-00:00:00
     * @throws Exception
     */
    public static function getTimeIntervalFromString(string $daysTime)
    {
        $daysTime = trim($daysTime);

        // Get time from string
        preg_match('/\b(\d{1,2}(:\d{2})*\s\w{2}\s-\s\d{1,2}(:\d{2})*\s\w{2})\b/', $daysTime, $timeMatches);

        if (!$timeMatches) {
            throw new Exception('No valid time interval found');
        }

        // Get valid time
        $timeInterval = explode('-', str_replace(' ', '', $timeMatches[0]));
        $timeInterval[0] = RestaurantHelper::formatTime($timeInterval[0]);
        $timeInterval[1] = RestaurantHelper::formatTime($timeInterval[1]);

        return implode('-', $timeInterval);
    }
}
