<?php

namespace console\controllers;

use common\models\User;
use Exception;
use Yii;

class UserController extends Controller
{
    public function actionIndex()
    {
        echo 'yii user/create-admin' . PHP_EOL;
    }

    public function actionCreateAdmin()
    {
        $username = 'admin';
        $password = '11111111';

        $isAdminExist = User::find()->where(['username' => $username])->exists();

        if ($isAdminExist) {
            return $this->out(Yii::t('app', 'Admin user has already exist, username: "{username}", password: "{password}"', [
                'username' => $username,
                'password' => $password,
            ]));
        }

        $message = '';

        try {
            $user = new User();
            $user->username = $username;
            $user->email = 'admin@example.com';
            $user->setPassword($password);
            $user->generateAuthKey();
            $user->status = User::STATUS_ACTIVE;

            if (!$user->save()) {
                throw new Exception(Yii::t('app', 'Error. Admin user has not been created'));
            }

            $message = Yii::t('app', 'Admin user has been created, username: "{username}", password: "{password}"', [
                'username' => $username,
                'password' => $password,
            ]);
        } catch (Exception $e) {
            $message = $e->getMessage();
        }

        return $this->out($message);
    }
}
