<?php

namespace console\controllers;

use yii\helpers\Console;

class Controller extends \yii\console\Controller
{
    /**
     * Выводит сообщение с переносом строки
     *
     * @param string $string
     *
     * @return bool|int
     */
    public function out(string $string)
    {
        return Console::output($string);
    }
}
