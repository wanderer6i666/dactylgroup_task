<?php

namespace console\controllers;

use common\helpers\RestaurantHelper;
use common\models\Restaurant;
use common\models\RestaurantSchedule;
use console\helpers\RestaurantImportHelper;
use Exception;
use Yii;

class RestaurantController extends Controller
{
    public function actionIndex()
    {
        echo 'yii restaurant/import' . PHP_EOL;
    }

    public function actionImport()
    {
        $this->baseImport();
        $this->scheduleImport();
    }

    protected function baseImport()
    {
        $importFile = Yii::getAlias('@console/import/restaurants-hours-source-1.csv');

        $countSaved = 0;
        $countIgnored = 0;

        try {
            $data = RestaurantImportHelper::readFileCsv($importFile, [
                'Restaurant name',
                'Restaurant ID',
                'Cuisine',
                'Opens',
                'Closes',
                'Days Open',
                'Price',
                'Rating',
                'Location',
                'Description',
            ]);

            // All columns
            $colNames = array_flip(array_shift($data));

            // Custom impot (could be imported by \ruskid\csvimporter\ARImportStrategy)
            foreach ($data as $line) {
                $transaction = Yii::$app->getDb()->beginTransaction();
                try {
                    $restaurant = new Restaurant();
                    $restaurant->name = $line[$colNames['Restaurant name']];
                    $restaurant->code = $line[$colNames['Restaurant ID']];
                    $restaurant->cuisine = $line[$colNames['Cuisine']];
                    $restaurant->price = $line[$colNames['Price']];
                    $restaurant->rating = $line[$colNames['Rating']];
                    $restaurant->location = $line[$colNames['Location']];
                    $restaurant->description = $line[$colNames['Description']];

                    if (!$restaurant->save()) {
                        throw new Exception('Can not save restaurant');
                    }

                    // Get working days array
                    $daysOpen = array_unique(explode(',', $line[$colNames['Days Open']]));

                    // Save restaurant schedule
                    if ($daysOpen) {
                        foreach ($daysOpen as $day) {
                            $day = RestaurantHelper::formatDay($day);

                            $schedule = new RestaurantSchedule();
                            $schedule->restaurant_id = $restaurant->id;
                            $schedule->day = $day;
                            $schedule->opens = $line[$colNames['Opens']];
                            $schedule->closes = $line[$colNames['Closes']];

                            if (!$schedule->save()) {
                                throw new Exception('Can not save restaurant schedule');
                            }
                        }
                    }

                    $countSaved++;
                    $transaction->commit();
                } catch (Exception $e) {
                    $countIgnored++;
                    $transaction->rollBack();
                }
            }
        } catch (Exception $e) {
            $this->out($e->getMessage());
            die;
        }

        $this->out(Yii::t('app', 'Base file imported. Count saved: {countSaved}. Count ignored: {countIgnored}', [
            'countSaved' => $countSaved,
            'countIgnored' => $countIgnored,
        ]));
    }

    protected function scheduleImport()
    {
        $importFile = Yii::getAlias('@console/import/restaurants-hours-source-2.csv');

        $countSaved = 0;
        $countIgnored = 0;

        try {
            $data = RestaurantImportHelper::readFileCsv($importFile);

            foreach ($data as $line) {
                $transaction = Yii::$app->getDb()->beginTransaction();
                try {
                    $restaurant = new Restaurant();
                    $restaurant->name = $line[0];
                    $restaurant->code = $line[0];

                    if (!$restaurant->save()) {
                        throw new Exception('Can not save restaurant');
                    }

                    // Get working days array
                    $daysTimeOpen = array_unique(explode('/', $line[1]));

                    foreach ($daysTimeOpen as $daysTime) {
                        // Get valid days
                        $daysOpen = RestaurantImportHelper::getDaysFromString($daysTime);
                        // Get valid time
                        $timeInterval = RestaurantImportHelper::getTimeIntervalFromString($daysTime);
                        $time = explode('-', $timeInterval);
                        $timeOpens = $time[0];
                        $timeCloses = $time[1];

                        foreach ($daysOpen as $dayOpen) {
                            $schedule = new RestaurantSchedule();
                            $schedule->restaurant_id = $restaurant->id;
                            $schedule->day = $dayOpen;
                            $schedule->opens = $timeOpens;
                            $schedule->closes = $timeCloses;

                            if (!$schedule->save()) {
                                throw new Exception('Can not save restaurant schedule');
                            }
                        }
                    }

                    $countSaved++;
                    $transaction->commit();
                } catch (Exception $e) {
                    $countIgnored++;
                    $transaction->rollBack();
                }
            }
        } catch (Exception $e) {
            $this->out($e->getMessage());
            die;
        }

        $this->out(Yii::t('app', 'Schedule file imported. Count saved: {countSaved}. Count ignored: {countIgnored}', [
            'countSaved' => $countSaved,
            'countIgnored' => $countIgnored,
        ]));
    }
}
